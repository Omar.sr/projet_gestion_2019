/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metiers;

import Daos.Dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author omar
 */
public class GestionReservation {
        private Dao dao ;
     private Connection con ;
     
    public  GestionReservation()
     {
         this.dao=new Dao("com.mysql.jdbc.Driver","jdbc:mysql://localhost:3306/bd_projet","root","");
            dao.Seconnecter();
            con=dao.getConnexion();
     }
     /**
      * 
      * Ajouts d'une reservation
      * @param numeroCh
      * @param cin
      * @param cin_Recep
      * @param datedebut
      * @param datefin
      * @return 
      * pour  reservation en ligne cin_Recep est null 
      */
      public int Ajouter_reservation(int numeroCh,String cin,String cin_Recep,Date datedebut,Date datefin)
      { 
                String req="INSERT INTO `reserver` (`NUMEROCH`, `CIN`, `CIN_E`, `DATEDEBUT`, `DATEFIN`) "
                        + "VALUES ("+numeroCh+",'"+cin+"', '"+cin_Recep+"', '"+new java.sql.Date(datedebut.getTime())+"', '"+ new java.sql.Date(datefin.getTime())+"')";
              return dao.executeDml(req);
      }
          
 /**
  * 
  * 
  * -Rechercher Les information de la Reservation d'une chambre par un client
   * 
 */ 
    public ResultSet RechercherRes (String cin , int numC )
    {
        
              java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
              String req="Select numeroch as N°chambre,cin,non,prenom, datedebut , datefin from reserver where cin = '"+cin+"' and numeroch = "+numC+" and datedebut < '"+date+"' and datefin >= '"+date+"' ";
           return dao.executeInterrogation(req);
    }
      
       
      
      
      
      
}
