/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metiers;

import Daos.Dao;
import java.sql.Connection;
import java.util.Date;

/**
 *
 * @author omar
 */
public class GestionPersonnel {
      private Dao dao ;
     private Connection con ;
     
    public GestionPersonnel()
     {
    this.dao=new Dao("com.mysql.jdbc.Driver","jdbc:mysql://localhost:3306/bd_projet","root","");
            dao.Seconnecter();
     }
    /**
     * Ajoute du Personnel 
     * @param cin_e
     * @param nom
     * @param prenom
     * @param Date_naissance
     * @param email
     * @param telephone
     * @param sexe
     * @param adresse
     * @param codepostal
     * @return 
     */
    public int ajouterPersonnel(String cin_e,String nom,String prenom,java.sql.Date Date_naissance, String email , String telephone,String sexe,String adresse,int codepostal )
    {
       String req= "INSERT INTO `personnel`(`CIN_E`, `NOM`, `PRENOM`, `DATE_NAISSANCE`, `EMAIL`, `TELEPHONE`, `SEXE`, `ADRESSE`, `CODEPOSTAL`) "
                + "VALUES ('"+cin_e+"','"+nom+"','"+prenom+"','"+Date_naissance+"','"+email+"','"+telephone+"','"+sexe+"','"+adresse+"','"+codepostal+"')";
       
       return dao.executeDml(req);
       
        
    }  
          
    public int ajouterReceptioniste(String cin_e,String nom,String prenom,java.sql.Date Date_naissance, String email , String telephone,String sexe,String adresse,int codepostal )
    {
       
       String req= "INSERT INTO `receptionistes`(`CIN_E`, `NOM`, `PRENOM`, `DATE_NAISSANCE`, `EMAIL`, `TELEPHONE`, `SEXE`, `ADRESSE`, `CODEPOSTAL`) "
                + "VALUES ('"+cin_e+"','"+nom+"','"+prenom+"','"+Date_naissance+"','"+email+"','"+telephone+"','"+sexe+"','"+adresse+"','"+codepostal+"')";
       
       return this.ajouterEmploye(cin_e, nom, prenom, Date_naissance, email, codepostal, sexe, adresse, codepostal)+dao.executeDml(req);
       
        
    }  
    public int ajouterGerant(String cin_e,String nom,String prenom,java.sql.Date Date_naissance, String email , String telephone,String sexe,String adresse,int codepostal )
    {
       String req= "INSERT INTO `gerant`(`CIN_E`, `NOM`, `PRENOM`, `DATE_NAISSANCE`, `EMAIL`, `TELEPHONE`, `SEXE`, `ADRESSE`, `CODEPOSTAL`) "
                + "VALUES ('"+cin_e+"','"+nom+"','"+prenom+"','"+Date_naissance+"','"+email+"','"+telephone+"','"+sexe+"','"+adresse+"','"+codepostal+"')";
       
       return this.ajouterEmploye(cin_e, nom, prenom, Date_naissance, email, codepostal, sexe, adresse, codepostal)+dao.executeDml(req);
       
        
    }  
    public int ajouterDirecteur(String cin_e,String nom,String prenom,java.sql.Date Date_naissance, String email , String telephone,String sexe,String adresse,int codepostal )
    {
       String req= "INSERT INTO `directeur`(`CIN_E`, `NOM`, `PRENOM`, `DATE_NAISSANCE`, `EMAIL`, `TELEPHONE`, `SEXE`, `ADRESSE`, `CODEPOSTAL`) "
                + "VALUES ('"+cin_e+"','"+nom+"','"+prenom+"','"+Date_naissance+"','"+email+"','"+telephone+"','"+sexe+"','"+adresse+"','"+codepostal+"')";
       
       return this.ajouterPersonnel(cin_e, nom, prenom, Date_naissance, email, telephone, sexe, adresse, codepostal)+dao.executeDml(req);
       
        
    }  
        public int ajouterEmploye(String cin_e,String nom,String prenom,java.sql.Date Date_naissance, String email , int telephone,String sexe,String adresse,int codepostal )
    {
     String req= "INSERT INTO employe VALUES ('"+cin_e+"','"+nom+"','"+prenom+"','"+Date_naissance+"','"+email+"','"+telephone+"','"+sexe+"','"+adresse+"','"+codepostal+"')";
            
     return this.ajouterPersonnel(cin_e, nom, prenom, Date_naissance, email, adresse, sexe, adresse, codepostal)+dao.executeDml(req);
    }
}
