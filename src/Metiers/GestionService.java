/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metiers;


import java.util.Date;
import Daos.Dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asus
 */
public class GestionService{
    private Connection con ;
    private Dao dao ;
    
    public GestionService()
    {
        this.dao=new Dao("com.mysql.jdbc.Driver","jdbc:mysql://localhost:3306/bd_projet","root","");
            dao.Seconnecter();
            con=dao.getConnexion();;
    }
    
  
    /**
     * Rechercher tout les Services concernant un client et une chambre-
     * 
     *
     */
    
    public int AjouterService(int id , float prix , String designation)
    {
        String req= "INSERT INTO `services`(`IDS`, `PRIX`, `DESIGNATION`) VALUES ("+id+","+prix+",'"+designation+"')";
        return dao.executeDml(req);
    }
     public int DeleteService(int id)
    {
        String req= "Delete from services where ids="+id+"";
        return dao.executeDml(req);
    }
     
     
     public Vector<String> getServices()
     {
          Vector<String> services=new Vector<String>();
         String req="select * from services";
         ResultSet rs=dao.executeInterrogation(req);
        try {
            while(rs.next())
            {
                services.add(rs.getInt(1)+"-"+rs.getString(3));
                
            }
            return services;
        } catch (SQLException ex) {
            Logger.getLogger(GestionService.class.getName()).log(Level.SEVERE, null, ex);
        }
         
         return null; 
     }
     
    public ResultSet RechercherServices (int cin , int numC )
    {
     
        
           
               java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
               String req="Select designation , prix , d.date from service AS s , demander AS d , reserver AS r where s.ids=d.ids and d.id_r=r.id_r and r.cin = '"+cin+"' and r.numeroch = "+numC+" and datedebut <'"+date+"' and datfin >='"+date+"'" ;
             
               return dao.executeInterrogation(req);
        
    }
    
/**
 * 
 * 
 * -Prix total des services concernant un client et une chambre
 */
    public ResultSet PrixTotaleServices (int cin , int numC )
    {
       
               java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
              String req ="Select sum(prix) as Prix Total  from service AS s , demander AS d , reserver AS r where s.ids=d.ids and d.id_r=r.id_r and r.cin = '"+cin+"' and r.numeroch = "+numC+" and datedebut <'"+date+"' and datfin >='"+date+"'" ;
           
              return dao.executeInterrogation(req);
    }
    /**
     * -Rechercher Les information de la Reservation d'une chambre par un client(affichage des service-
     * 
     * */
    public ResultSet RechercherRes (String cin , int numC )
    {
        ResultSet res = null ;
        
          
              java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
               String req="Select numeroch as numero , datedebut as date debut as , datefin as date fin from reserver where cin ='"+cin+"' and numeroch = "+numC+" and datedebut < '"+date+"' and datefin >= '"+date+"'";
               return dao.executeInterrogation(req);
              
    }
    
}