/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author omar
 */
public class Dao {
  private  String Pilote ;
    private  String Url ;
    private  String Login ;
    private  String  Password ;
     private  Connection Connexion ; 
     
     
    public Dao(String Pilote , String Url,String Login , String Password)
    {
     this.Pilote=Pilote ; 
     this.Url=Url;
     this.Login =Login ; 
     this.Password  = Password ; 
     this.Seconnecter();
     
        
    }
    public void Seconnecter ()
    {
        try {
            Class.forName(Pilote);
            System.out.println("Chargement Pilote reussi" );
            Connexion=DriverManager.getConnection(Url, Login, Password);
            System.out.println("Connextion etablie ");
        } catch (ClassNotFoundException ex) {
                     System.err.println("Chargment de Pilote Echoue "+ex.getMessage());

        } catch (SQLException ex) {
              System.err.println("Connnextion  Echoue "+ex.getMessage());
        }
        
        
    }

    public String getPilote() {
        return Pilote;
    }

    public String getUrl() {
        return Url;
    }

    public String getLogin() {
        return Login;
    }

    public String getPassword() {
        return Password;
    }

    public Connection getConnexion() {
        return Connexion;
    }

    public void setPilote(String Pilote) {
        this.Pilote = Pilote;
    }

    public void setUrl(String Url) {
        this.Url = Url;
    }

    public void setLogin(String Login) {
        this.Login = Login;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public void setConnexion(Connection Connexion) {
        this.Connexion = Connexion;
    }
            
    public ResultSet executeInterrogation(String req)
    {   
             Dao dao=new Dao("com.mysql.jdbc.Driver","jdbc:mysql://localhost:3306/bd_projet","root","");
            dao.Seconnecter();
            try 
        {
            Statement st;
                 st = dao.Connexion.createStatement();
           
            
            ResultSet rs=st.executeQuery(req);
            return rs ;
        }
        catch( SQLException ex)
        {
            System.err.println(ex.getMessage());
            return null ;
        }      
        
   
        
    }
   
    public int executeDml(String req)
    {
          Dao dao=new Dao("com.mysql.jdbc.Driver","jdbc:mysql://localhost:3306/bd_projet","root","");
            dao.Seconnecter();
            int i=0;
            try 
            {
                 Statement st;
                  st = dao.Connexion.createStatement();
                  i=st.executeUpdate(req);
            } catch (SQLException ex) {
            System.err.println(ex.getMessage());
      }
      return i;
        
    }
}
